
var canvas = document.getElementById('mycanvas');
canvas.width = "1000";
canvas.height = "600";
var musshape = 1;

var ctx = canvas.getContext('2d');
var nowx;
var nowy;
var shaping=0;
var isfill=0;
var colormove;
var pos;
var postri;
var tmp1;
var tmp2;
var tmp3;
var cnt=0;
var j=0;
var cPushArray = new Array();
var cStep = -1;
var a=0;
var b=3;
var color
var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black',  'gray'];
var size = [1, 3, 5, 10, 15, 30];
var sizeNames = ['default', 'three', 'five', 'ten', 'fifteen', 'thirty'];
var fontname =['Impact','Arial','Courier','Palatino','Garamond'];
//ctx.arc(100,75,50,0,2*Math.PI);
//ctx.fillRect(100,100,500,200);
//ctx.clearRect(45,45,60,60);
//ctx.strokeRect(50,50,50,50);
//ctx.stroke();

cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('mycanvas').toDataURL());
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function mouseMove(evt) {
    var mousePos = getMousePos(canvas, evt);
    if(shaping===0){
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
    }
    if(shaping===1){
        color = document.getElementById("colorselect").value;
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        postri=Math.abs(((mousePos.x)+nowx)/2);
        ctx.putImageData(tmp1,0 ,0);
        ctx.beginPath();
        ctx.moveTo(postri,nowy);
        ctx.lineTo(nowx,mousePos.y);
        ctx.lineTo(mousePos.x,mousePos.y);
        if(isfill%2==1){
            ctx.fill();
        }
        ctx.closePath();
        ctx.stroke();
    }
    if(shaping===2){
        color = document.getElementById("colorselect").value;
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        ctx.putImageData(tmp2,0 ,0);
        ctx.strokeRect(nowx,nowy,(mousePos.x)-nowx,(mousePos.y)-nowy);
        if(isfill%2==1){
            ctx.fillRect(nowx,nowy,(mousePos.x)-nowx,(mousePos.y)-nowy);
        }
    }
    if(shaping===3){
        color = document.getElementById("colorselect").value;
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        ctx.putImageData(tmp3,0 ,0);
        pos =Math.abs((mousePos.x)-nowx);
        ctx.beginPath();
        ctx.arc(nowx,nowy,pos,0,2*Math.PI);
        if(isfill%2==1){
            ctx.fill();
        }
        ctx.closePath();
        ctx.stroke();
    }
    ctx.lineCap = "round";
    //ctx.lineCap = "butt";
    
}

canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);
    nowx = mousePos.x;
    nowy = mousePos.y;
    canvas.addEventListener('mousemove', mouseMove, false);
    if(colormove!=1){
        color = document.getElementById("colorselect").value;
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        
    }
});

canvas.addEventListener('mouseup', function() {
    tmp1 = ctx.getImageData(0,0,canvas.width,canvas.height);
    tmp2 = ctx.getImageData(0,0,canvas.width,canvas.height);
    tmp3 = ctx.getImageData(0,0,canvas.width,canvas.height);
    if(shaping==4){
        console.log("000");
        ctx.lineWidth =1;
        //ctx.font = "20pt Arial";
        ctx.font =size[b]+"px "+ fontname[a];
        console.log(ctx.font);
        if(isfill==0){
            ctx.strokeText(document.getElementById("tt").value ,nowx ,nowy);
        }
        if(isfill==1){
            ctx.fillText(document.getElementById("tt").value ,nowx ,nowy);
        }
    }
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('mycanvas').toDataURL());
    canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

document.getElementById('b1').addEventListener('click', function() {
    ctx.strokeStyle ='black';
    document.getElementById("mycanvas").style.cursor = 'url(img/Pen.png) 0 30,auto';
    shaping=0;
    colormove=0;
}, false);

document.getElementById('b2').addEventListener('click', function() {
    ctx.strokeStyle ='white';
    document.getElementById("mycanvas").style.cursor = 'url(img/Eraser.png) 0 30,auto';
    shaping=0;
    colormove=1;
}, false);

var canvasPic;

document.getElementById('b3').addEventListener('click', function() {
    console.log(cStep)
    if (cStep > 0) {
        cStep--;
        canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.fillStyle = "rgb(255,255,255)"
            ctx.fillRect(0,0,canvas.width,canvas.height)
            ctx.drawImage(canvasPic, 0, 0); }
    }
}, false);

document.getElementById('b4').addEventListener('click', function() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        console.log("@");
    }
}, false);

document.getElementById('b5').addEventListener('click', function() {
    shaping=1;
    document.getElementById("mycanvas").style.cursor = 'url(img/triangle.png) 0 30,auto';
    tmp1 = ctx.getImageData(0,0,canvas.width,canvas.height);
    colormove=0;
}, false);

document.getElementById('b6').addEventListener('click', function() {
    shaping=2;
    document.getElementById("mycanvas").style.cursor = 'url(img/rectangle.png) 0 30,auto';
    tmp2 = ctx.getImageData(0,0,canvas.width,canvas.height);
    colormove=0;
}, false);

document.getElementById('b7').addEventListener('click', function() {
    shaping=3;
    document.getElementById("mycanvas").style.cursor = 'url(img/circle.png),auto';
    tmp3 = ctx.getImageData(0,0,canvas.width,canvas.height);
    colormove=0;
}, false);

document.getElementById('b8').addEventListener('click', function() {
    isfill=isfill+1;
}, false);


function doInput(id){
    var inputObj = document.createElement('input');
    inputObj.addEventListener('change',readFile,false);
    inputObj.type = 'file';
    inputObj.accept = 'image/*';
    inputObj.id = id;
    inputObj.click();
}


function readFile(){
    var file = this.files[0];//獲取input輸入的圖片
    //if(!/image\/\w /.test(file.type)){
       // alert("請確保檔案為影象型別");
       // return false;
    //}//判斷是否圖片，在移動端由於瀏覽器對呼叫file型別處理不同，雖然加了accept = 'image/*'，但是還要再次判斷
    console.log("111");
    var reader = new FileReader();
    reader.readAsDataURL(file);//轉化成base64資料型別
    reader.onload = function(e){
        //console.log("444");
        drawToCanvas(this.result);
    }
}

function drawToCanvas(imgData){
    var cvs = document.querySelector('#mycanvas');
    console.log("1234");
    cvs.width=1000;
    cvs.height=600;
    var ctx = cvs.getContext('2d');
    var img = new Image;
    img.src = imgData;
    img.onload = function(){//必須onload之後再畫
        ctx.drawImage(img,0,0,1000,600);
        strDataURI = cvs.toDataURL();//獲取canvas base64資料
        cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('mycanvas').toDataURL());
    }
}

function downloadimage(obj) {
    obj = canvas.toDataURL();
}

document.getElementById('b11').addEventListener('click', function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('mycanvas').toDataURL());
}, false);

document.getElementById('b12').addEventListener('click', function() {
    shaping=4;
    document.getElementById("mycanvas").style.cursor = "text"; 
    colormove=0;
}, false);

function listener(i) {
    document.getElementById('colorselect').addEventListener('click', function() {
        if(colormove!=1){
            color = document.getElementById("colorselect").value;
            ctx.strokeStyle = color;
            ctx.fillStyle = color;
            
        }
    }, false);
}

function fontSizes(i) {
    document.getElementById(sizeNames[i]).addEventListener('click', function() {
        ctx.lineWidth = size[i];
        b=i;
        //a=size[i];
    }, false);
}

function fontt(i) {
    document.getElementById(fontname[i]).addEventListener('click', function() {
        a=i;
        //ctx.font ="30pt" + fontname[i];
        //ctx.font = "20pt Arial";
        //console.log(size[i]);
        //console.log(ctx.font);
    }, false);
}


for(var i = 0; i < colors.length; i++) {
    listener(i);
}

for(var i = 0; i < size.length; i++) {
    fontSizes(i);
}

for(var i = 0; i < fontname.length; i++) {
    fontt(i);
}