# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y  (color selector用html內建)|
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | N         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    在網頁上，空白的地方是畫布，在點擊painter即可作畫，一開始的顏色會是調色盤的初始顏色，可經由調色改變畫筆的顏色。
    而各個按鍵則是跟作業要求的相同。在Text部分，則是用先打好要騰上去的文字，再用畫筆點擊即可印出字。

### Function description

    因為html和css為架構和美觀，我就著重講js的邏輯部分。
    js的部分除了宣告之外，有一個getmousePos可以隨時知道鼠標當前位置，然後有好幾個eventlistener(有mousedown,mousemove跟mouseup))藉由這幾個就可以隨時拉線從起始倒數標的位置。
    第一個畫筆的功能，就是在按下btn後(有listener)，設定線條顏色是Color Selector的起始顏色(紅色)，然後藉由上述的function就可以劃出線條了。
    eraser的功能就只是把顏色設定成白色，另外我有用一個變數colormove不讓eraser換顏色。
    redo 跟undo就是有用一個陣列存下來，然後記錄每一步(用cStep控制)undo就回前一個(--)，redo就到後一個(++)，然後畫出來。
    接著畫圖形，我有用一個變數shaping來判斷是畫圓、畫方、還是三角形，然後當然要記得beginpath跟clothpath，方型好像比較特殊不用，然後我是在btn按下時紀錄圖畫，之後一邊畫一邊把之前存的蓋上去，看起來就像大家看到的那樣了，然後在mousup的地方再存一次圖畫。這個時候我讓畫筆跟橡皮擦的shaping=0
    然後不讓畫線，就可以完整的畫出圖形了。
    然後有用一個變數isfill當基數，就畫實心，偶數就畫空心。
    接著是upload，就是用ㄧ些function，在按下btn的時候呼叫，主要是doInput、readFile跟drawToCanvas，在畫出來之後還要用cStep紀錄一下，這也算是一個步驟，可以redo跟undo。
    下載的部份不知道為何一直跑不出來，可能是我有地方疏忽了，還請助教指點。
    reset的部分就很簡單了，就是按下btn後，clear掉canvas，不過一樣用cStep紀錄，這也是一步驟。
    字體的部分是在html那邊用一個input，然後再在js的地方當按下btn時，就可以顯示出來，這部分我是打在mouseup的地方，然後把shaping=4;，這個時候前面的線條粗細會被固定成1，然後數字按鈕會變成字體大小，再加上字型的按鈕，就可以改字體大小跟字型了。
    color跟粗細還有字體的部分較也很簡單，用個for把每個btn對上，當listener時就可以直接用array的部分對上取用。

### Gitlab page link

     "https://106060022.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    遠端lab辛苦助教了，助教加油~

<style>
table th{
    width: 100%;
}
</style>